Installing:

1) cd /var/www/   /*директория в к-й находятся файлы должна быть корнем проекта*/

2) git clone https://oleg-kh-ua@bitbucket.org/oleg-kh-ua/mystore.git


Running:

1) Открываем на localhost: работает, но выдает ошибку SQL

2) Делаем импорт базы из файла phpshop.sql, который прикреплен к письму на почте

3) Меняем настройки подключения к бд в файле: mystore/config/db_params.php

4) Включаем mod_rewrite в ubuntu, если он у Вас не включен: http://codematrix.ru/%D0%97%D0%B0%D0%BF%D0%B8%D1%81%D0%BA%D0%B8/ubuntu/60-%D0%92%D0%BA%D0%BB%D1%8E%D1%87%D0%B5%D0%BD%D0%B8%D0%B5-mod_rewrite-%D0%B2-ubuntu.html